# MapIgn

Use IGN map for map question.

## Installation and sage

1. Extract the download and upload the "MapIgn" folder to ./upload/themes/question/.
2. Create an MapIgn question (in Mask question)

### Question Settings

Some settings related to map are hidden, since it can not be used or uneeded with this theme.

Only 4 settings are currently used

- _Get default location from browser_ : Get the location from browser when page load. User can always use Locate me button.
- _Zoom level_ : The inital zoom level
- _Default position_ : Default coordinates of the map when the page first loads. Format: 'latitude longtitude", latitude and longitude 
- _Map height_ : The Map height in pixel, width is always set to 100%

### Participant usage

Participant see the map using [OrthoPhotos](https://www.geoportail.gouv.fr/) for layer. To allow usage of location from browser : user must allow browser to get position.

A new button is added **Locate me** allowing to set the position to the current location at any time.

Participant can update layer and choose

- [OrthoPhotos](https://www.geoportail.gouv.fr/)
- [Plan IGN](https://www.ign.fr/)
- [OpenStreetMap (OSM)](https://www.openstreetmap.org/)
- [Plan IGN](https://www.ign.fr/) with [OrthoPhotos](https://www.geoportail.gouv.fr/)
- [OpenStreetMap (OSM)](https://www.openstreetmap.org/) with [OrthoPhotos](https://www.geoportail.gouv.fr/)

### Use your own IGN API key

By default the Questin theme use [essentiels](https://geoservices.ign.fr/services-web-essentiels) API key.

You can set to your own API key updating confoig.php file and add the ignApiKey as your own config :

```
    'config'=>array(
		'debug'=>2,
		'debugsql'=>0, // Set this to 1 to enanble sql logging, only active when debug = 2
		// Mysql database engine (INNODB|MYISAM):
		'mysqlEngine' => 'MYISAM',
		// Update default LimeSurvey config here
		'ignApiKey' => "Your own IGN API key",
	)
```

### Usage with [OfflineSurveys](https://www.offlinesurveys.com/)

A specific version for OfflineSurveys are compatible with this Question theme, allowing to use geoportail OrthoPhotos and IGN Map in such survey.

You can contact OfflineSurveys support if you need such solution.


## Copyright and home page

- HomePage <https://extensions.sondages.pro>
- Copyright © 2022-2023 Denis Chenu <https://sondages.pro>
- Copyright © 2022-2023 Dr. Marcel Minke <https://www.offlinesurveys.com>
- Licence : GNU AFFERO GENERAL PUBLIC LICENSE <https://www.gnu.org/licenses/agpl-3.0.html>
- Code, issue and feature : [gitlab](https://gitlab.com/SondagesPro/QuestionTheme/MapIgn)
