/*
 * This file is part of LimeSurvey adapted for IGN
 * @license Affero GPL
 * See COPYRIGHT.php for copyright notices and details.
 * @version 1.0.0
 */


function isvalidCoord(val,type){
    if(type === 'lat'){
        var min = -90;
        var max = 90
    }else{
        var min = -180;
        var max = 180
    }
    if (!isNaN(parseFloat(val)) && (val>min && val<=max)) {
        return true;
    } else {
        return false;
    }
}


// OSMap functions
function IGNGeoInitialize(basename,MapOptions){
        let tileServerURL = {
            OSM : "//{s}.tile.openstreetmap.org/{z}/{x}/{y}",
            HUM : "//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}",
            CYC : "//{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}",
            TRA : "//{s}.tile.thunderforest.com/transport/{z}/{x}/{y}"
        };
        let latLng = MapOptions.defaultcoordinates.split(" ");
        let latitude = latLng[0];
        let longitude = latLng[1];
        // If not latLng is set the Map will center to France
        if(!isvalidCoord(latitude, 'lat')){
            latitude=46.227638;
        }
        if(!isvalidCoord(longitude, 'lng')){
            longitude=2.213749;
        }
       
        MapOptions.latitude = latitude;
        MapOptions.longitude = longitude;
         /* get by current value */
        let lat = -9999;
        let lng = -9999;
        let currentLocation = $("#answer" + basename).val().trim();
        if (currentLocation) {
            let currentLatLng = currentLocation.split(";");
            if(currentLatLng.length == 2) {
                lat = currentLatLng[0];
                lng = currentLatLng[1];
                if (isvalidCoord(lat, 'lat') && isvalidCoord(lng, 'lng')) {
                    MapOptions.latitude = lat;
                    MapOptions.longitude = lng;
                } else {
                    lat=-9999; lng=-9999;
                }
            }
        }
        // <mapOrthosAdd>
        let mapOrthos = L.tileLayer('https://data.geopf.fr/wmts?' +
            'REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE={style}&TILEMATRIXSET=PM' + 
            '&FORMAT={format}&LAYER={ignLayer}&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', {
            attribution: '<a target="_blank" href="https://www.geoportail.gouv.fr/">IGN-F/Géoportail</a>',
            bounds: [[-75, -180], [81, 180]],
            minZoom: 2,
            maxZoom: 19,
            ignLayer: 'ORTHOIMAGERY.ORTHOPHOTOS',
            format: 'image/jpeg',
            style: 'normal'
        });
        // </mapOrthosAdd>
        // <mapScan25Add>
        let PlanIGN = L.tileLayer('https://data.geopf.fr/wmts?'+
            '&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&TILEMATRIXSET=PM'+
            '&LAYER={ignLayer}&STYLE={style}&FORMAT={format}'+
            '&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}', {
            attribution: 'Data © <a target="_blank" href="https://www.ign.fr/">IGN France</a>',
            bounds: [[-75, -180], [81, 180]],
            ignLayer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
            format: 'image/png',
            service: 'WMTS',
            style: 'normal',
            opacity: 1
        });
        let PlanOrtho = L.layerGroup([
            L.tileLayer('https://data.geopf.fr/wmts?'+
            '&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&TILEMATRIXSET=PM'+
            '&LAYER={ignLayer}&STYLE={style}&FORMAT={format}'+
            '&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}', {
                attribution: '<a target="_blank" href="https://www.geoportail.gouv.fr/">IGN-F/Géoportail</a>',
                bounds: [[-75, -180], [81, 180]],
                ignLayer: 'ORTHOIMAGERY.ORTHOPHOTOS',
                format: 'image/jpeg',
                service: 'WMTS',
                style: 'normal'
            }),
            L.tileLayer('https://data.geopf.fr/wmts?'+
            '&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&TILEMATRIXSET=PM'+
            '&LAYER={ignLayer}&STYLE={style}&FORMAT={format}'+
            '&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}', {
                attribution: 'Data © <a target="_blank" href="https://www.ign.fr/">IGN France</a>',
                bounds: [[-75, -180], [81, 180]],
                ignLayer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
                format: 'image/png',
                service: 'WMTS',
                style: 'normal',
                opacity: 0.4
            })
        ]);
        let mapOrthosOSM = L.layerGroup([
            L.tileLayer('https://data.geopf.fr/wmts??REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE={style}&TILEMATRIXSET=PM&FORMAT={format}&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', {
                attribution: '<a target="_blank" href="https://www.geoportail.gouv.fr/">IGN-F/Géoportail</a>',
                bounds: [[-75, -180], [81, 180]],
                minZoom: 2,
                maxZoom: 19,
                format: 'image/jpeg',
                style: 'normal'
            }),
            L.tileLayer(tileServerURL.OSM+".png", {
                maxZoom: 19,
                subdomains: ["a", "b", "c"],
                attribution: 'Map data © <a href="//www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.',
                opacity: 0.4
            }),

        ]);
        // </mapScan25Add>
        let mapOSM = L.tileLayer(tileServerURL.OSM+".png", {
            maxZoom: 19,
            subdomains: ["a", "b", "c"],
            attribution: 'Map data © <a href="//www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
        });
        let mapCYC = L.tileLayer(tileServerURL.CYC+".png", {
            maxZoom: 19,
            subdomains: ["a", "b", "c"],
            attribution: 'Map data © <a href="//www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
        });
        let mapHOT = L.layerGroup([L.tileLayer(tileServerURL.HUM+".png", {
            maxZoom: 20,
            subdomains: ["a", "b", "c"],
        }), L.tileLayer(tileServerURL+".png", {
            maxZoom: 19,
            subdomains: ["a", "b", "c"],
            attribution: 'Map data © <a href="//www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
        })]);
        let mapTRA = L.layerGroup([L.tileLayer(tileServerURL.TRA+".png", {
            maxZoom: 19,
            subdomains: ["a", "b", "c"],
        }), L.tileLayer(tileServerURL+".png", {
            maxZoom: 19,
            subdomains: ["a", "b", "c"],
            attribution: 'Map data © <a href="//www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
        })]);

        let baseLayers = {
            "OrthoPhotos" : mapOrthos,   // <mapOrthosAdd>
            "Plan IGN" : PlanIGN,   // <mapScan25>
            "Plan / Ortho" : PlanOrtho,  // <mapScan25>
            "OSM / Ortho" : mapOrthosOSM,   // <mapOrthosAdd>
            "OpenStreetMap (OSM)": mapOSM,
        };
        let overlays = {
        };
        let map = L.map("map_"+basename, {
            zoom:MapOptions.zoomLevel,
            minZoom:1,
            center: [MapOptions.latitude, MapOptions.longitude] ,
            maxBounds: ([[-90, -180],[90, 180]]),
            layers: [mapOrthos]
        });


        var marker = new L.marker([lat,lng], {title:'Current Location',id:1,draggable:'true'});
        map.addLayer(marker);

        var layerControl = L.control.layers(baseLayers, overlays, {
          collapsed: true
        }).addTo(map);

        map.on('click',
            function(e) {
                var coords = L.latLng(e.latlng.lat,e.latlng.lng);
                if(isvalidCoord(coords.lat,'lat') && isvalidCoord(coords.lng,'lng')){
                    marker.setLatLng(coords);
                    UI_update(e.latlng.lat,e.latlng.lng);
                    $("#searchbox_"+basename).val("");
                }
            }
        )

        marker.on('dragend', function(e){
                var marker = e.target;
                var position = marker.getLatLng();
                UI_update(position.lat,position.lng);
                $("#searchbox_"+basename).val("");
        });

        function UI_update(lat,lng){
            if (isvalidCoord(lat,'lat') && isvalidCoord(lng,'lng')) {
                $("#answer"+basename).val(Math.round(lat*100000)/100000 + ";" + Math.round(lng*100000)/100000);
                $("#answer_lat"+basename).val(Math.round(lat*100000)/100000).removeClass("text-danger").data('prevvalue',Math.round(lat*100000)/100000);
                $("#answer_lng"+basename).val(Math.round(lng*100000)/100000).removeClass("text-danger").data('prevvalue',Math.round(lng*100000)/100000);
                $("#answer"+basename).val(lat + ";" + lng);
                map.setView([lat, lng]);
                marker.setLatLng(L.latLng(lat,lng));
            } else {
                $("#answer"+basename).val("");
                $("#answer_lat"+basename).val("").data('prevvalue','');
                $("#answer_lng"+basename).val("").data('prevvalue','');
            }
            checkconditions($("#answer"+basename).val(), name, 'text', 'keyup');
        }


        $('.coords[name^='+basename+']').each(function(){
            $(this).data('prevvalue',$(this).val());
        });
        /* Update from blur to change for Android */
        $('.coords[name^='+basename+']').on('change',function(){
            if ($(this).data('prevvalue') != $(this).val()) {
                var newLat = $("#answer_lat"+basename).val();
                if(newLat!=="" && !isvalidCoord(newLat,'lat')){
                    $("#answer_lat"+basename).addClass("text-danger");
                }else{
                    $("#answer_lat"+basename).removeClass("text-danger");
                }
                var newLng = $("#answer_lng"+basename).val();
                if(newLng!="" && !isvalidCoord(newLng)){
                    $("#answer_lng"+basename).addClass("text-danger");
                }else{
                    $("#answer_lng"+basename).removeClass("text-danger");
                }
                if (isvalidCoord(newLat,'lat') && isvalidCoord(newLng,'lng')) {
                    UI_update(newLat,newLng);
                    $("#searchbox_"+basename).val("");
                }
            }
            $(this).data('prevvalue',$(this).val());
        });
        /* get frombrowser */
        if(lat=-9999 && MapOptions.getfrombrowser) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(location) {
                    newLat = location.coords.latitude;
                    newLng = location.coords.longitude;
                    if (isvalidCoord(newLat,'lat') && isvalidCoord(newLng,'lng')) {
                        UI_update(newLat,newLng);
                    }
                });
            }
            // ANDROID NOT TESTED
            if (typeof Android !== 'undefined' && Android.getVersionCode()) {
                try {
                  Android.resolveUserLocation("answer_lat"+basename, "answer_lng"+basename);
                } catch (error) {
                  // No action
                }
            }
        }
        $("button[data-asklocation='map_" + basename + "']").on('click', function(){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(location) {
                    newLat = location.coords.latitude;
                    newLng = location.coords.longitude;
                    if (isvalidCoord(newLat,'lat') && isvalidCoord(newLng,'lng')) {
                        UI_update(newLat,newLng);
                        $("#searchbox_"+basename).val("");
                    }
                });
            }
            // ANDROID NOT TESTED
            if (typeof Android !== 'undefined' && Android.getVersionCode()) {
                try {
                  Android.resolveUserLocation("answer_lat"+basename, "answer_lng"+basename);
                } catch (error) {
                  // No action
                }
            }
        });
        var geonamesApiUrl = "api.geonames.org";
        if(window.location.protocol == 'https:'){
            /* Checked : work on 2019-03 , see #13873 */
            geonamesApiUrl = "secure.geonames.org";
        }
        $("#searchbox_"+basename).autocomplete({
            serviceUrl : "//"+geonamesApiUrl+"/searchJSON",
            dataType: "json",
            paramName: 'name_startsWith',
            deferRequestBy: 500,
            params:{
                username : IGNmap.geonameUser,
                featureClass : 'P',
                orderby : 'population',
                maxRows : 10,
                lang : IGNmap.geonameLang
            },
            ajaxSettings:{
                beforeSend : function(jqXHR, settings) {
                    if($("#restrictToExtent_"+basename).prop('checked')){
                        settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
                    }
                }
            },
            orientation: 'auto',
            minChars: 3,
            autoSelectFirst:true,
            transformResult: function(response) {
                return {
                    suggestions: $.map(response.geonames, function(geoname) {
                        return { value: geoname.name + " - " + geoname.countryName, data: { src:'geoname',lat:geoname.lat,lng:geoname.lng } };
                    })
                };
            },
            onSearchStart: function(query) {
                $( this ).prop("readonly",true);
            },
            onSearchComplete : function(query, suggestions) {
                $( this ).prop("readonly",false);
            },
            onSelect : function(suggestion) {
                if(suggestion.data.src == 'geoname'){
                    map.setView([suggestion.data.lat, suggestion.data.lng], MapOptions.zoomLevel);
                    UI_update(suggestion.data.lat, suggestion.data.lng);
                }
            }
        });
        var mapQuestion = $('#question'+name.split('X')[2]);

        function resetMapTiles(mapQuestion) {
            //window.setTimeout(function(){
                if($(mapQuestion).css('display') == 'none' && $.support.leadingWhitespace) { // IE7-8 excluded (they work as-is)
                    $(mapQuestion).css({
                        'position': 'relative',
                        'left': '-9999em'
                    }).show();
                    map.invalidateSize();
                    $(mapQuestion).css({
                        'position': 'relative',
                        'left': 'auto'
                    }).hide();
                }

            //},50);
        }

        resetMapTiles(mapQuestion);

        jQuery(window).resize(function() {
            window.setTimeout(function(){
                resetMapTiles(mapQuestion);
            },5);
        });

    /* Remove the cache from search when click on restrictToExtent */
    $("#restrictToExtent_"+basename).on("change",function(){
        $("#searchbox_"+basename).autocomplete('clearCache');
    });
    /* if restrictToExtent is checked : remove the search cache when bound updated */
    $("#searchbox_"+basename).on("viewreset",function(){ /* moveend,zoomend */
        if($("#restrictToExtent_"+basename).is(":checked")){
            $("#searchbox_"+basename).autocomplete('clearCache');
        }
    });
    /* reset search on focus */
    $("#searchbox_"+basename).on("focusin",function(){
        $(this).val("");
    });
    return map;
}
